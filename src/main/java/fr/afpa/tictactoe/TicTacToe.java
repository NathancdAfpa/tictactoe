package fr.afpa.tictactoe;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

/**
 * Classe principale d'un jeu morpion simple.
 * Le tour par tour n'est pas géré.
 * 
 * Il est simplement possible d'ajouter une croix dans la grille avec un clic gauche et d'ajouter un cercle avec un clic droit.
 */
public class TicTacToe extends Application {

    private final String windowTitle = "Jeu du morpion";

    /**
     * La taille du plateau de jeu.
     */
    private final int size = 3;

    /**
     * Plateau de jeu, size*size cases
     */
    private final BoardSquare[][] board = new BoardSquare[size][size];

    /**
     * Objet de la classe encapsulant la logique du jeu.
     */
    private final GameLogic logic = new GameLogic(board);

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) {
        BorderPane border = new BorderPane();
        HBox control = new HBox();
        control.setPrefHeight(40);
        control.setSpacing(10.0);
        control.setAlignment(Pos.BASELINE_CENTER);

        Button start = new Button("Recommencer");
        start.setOnMouseClicked(event -> border.setCenter(this.buildBoard()));

        control.getChildren().add(start);
        // TODO ajouter un bouton "Quitter" pour arrêter le programme
        border.setBottom(control);
        border.setCenter(this.buildBoard());
        stage.setScene(new Scene(border, 200, 200));
        stage.setTitle(windowTitle);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Affiche un message d'alert
     * @param message Le texte du message
     */
    private void showAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(windowTitle);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Construit une case du plateau de jeu.
     * @param x
     * @param y
     * @param size
     * @return
     */
    private BoardSquare buildSquare(int x, int y, int size) {
        BoardSquare rect = new BoardSquare();
        rect.setX(x * size);
        rect.setY(y * size);
        rect.setHeight(size);
        rect.setWidth(size);
        rect.setFill(Color.WHITE);
        rect.setStroke(Color.BLACK);
        return rect;
    }

    /**
     * Construction d'un plateau de jeu.
     */
    private Group buildBoard() {
        Group panel = new Group();
        for (int y = 0; y != this.size; y++) {
            for (int x = 0; x != this.size; x++) {
                BoardSquare square = this.buildSquare(x, y, 50);
                this.board[y][x] = square;
                panel.getChildren().add(square);
                // ajout d'un gestionnaire d'évènement à une de l'interface
                // l'évènement est instancié dans la méthode buildMouseEvent
                square.setOnMouseClicked(this.buildMouseEvent(panel));
            }
        }
        return panel;
    }

    /**
     * Construit des lignes représentant une croix.
     * @return Un groupe (qui est le conteneur le plus simple) contenant une croix à afficher
     */
    private Group buildMarkX(double x, double y, int size) {
        Group group = new Group();
        Line line1 = new Line(x + 10, y  + 10, x + size - 10, y + size - 10);
        Line line2 = new Line(x + size - 10, y + 10, x + 10, y + size - 10);

        // TODO augmenter l'épaisseur des lignes pour que les croix soient plus visibles
        // TODO modifier la couleur des lignes (couleur au choix) 

        group.getChildren().add(line1);
        group.getChildren().add(line2);
        return group;
    }

    /**
     * Construit un cercle.
     * @param x Le centre du cercle en abscisse
     * @param y Le centre du cercle en ordonnée
     * @param size le Diamètre du cercle
     * @return Un groupe (qui est le conteneur le plus simple) contenant un cercle à afficher
     */
    private Group buildMarkO(double x, double y, int size) {
        Group group = new Group();
        int radius = size / 2;

        Circle circle = new Circle(x + radius, y + radius, radius - 10);
        circle.setStroke(Color.BLACK);
         // TODO modifier la couleur des lignes (couleur au choix) 
        circle.setFill(Color.WHITE);
        group.getChildren().add(circle);
        return group;
    }

    /**
     * Construction d'un gestionnaire d'évènement pour gérer le clic de l'utilisateur sur un carré du plateau de jeu.
     * @param panel
     * @return
     */
    private EventHandler<MouseEvent> buildMouseEvent(Group panel) {
        return event -> {
            // récupération du composant graphique à l'origine de l'évènement
            BoardSquare square = (BoardSquare) event.getTarget();
            // TODO ajouter l'impossibilité de redessiner sur une case
            if (this.checkState()) {
                if (event.getButton() == MouseButton.PRIMARY) {
                    // la case devient propriété du joueur 1 (croix)
                    square.take(true);
                    panel.getChildren().add(this.buildMarkX(square.getX(), square.getY(), 50));
                } else {
                    // la case devient propriété du joueur 1 (cercle)
                    square.take(false);
                    panel.getChildren().add(this.buildMarkO(square.getX(), square.getY(), 50));
                }
                this.checkWinner();
                this.checkState();
            }
        };
    }

    /**
     * Vérifie si toutes les cases sont remplies
     * @return
     */
    private boolean checkState() {
        boolean gap = this.logic.hasGap();
        if (!gap) {
            this.showAlert("Toutes les cases sont remplies !");
        }
        return gap;
    }

    private void checkWinner() {
        if (this.logic.isWinnerX()) {
            this.showAlert("Le joueur 1 (croix) a gagné !");
        } else if (this.logic.isWinnerO()) {
            this.showAlert("Le joueur 2 (cercle) a gagné !");
        }
    }
}
